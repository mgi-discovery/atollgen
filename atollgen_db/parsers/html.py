#!/usr/bin/env python
# coding=utf-8

"""
>>> for island in parse_iceberg_files("data/inputs/iceberg/"):
...     print(island)

>>> for island in parse_paidb_files("../data/inputs/PAIDB_PAI.html", "../data/inputs/PAIDB-PAI_pages/"):
...     print(island)
"""


import json
from typing import Optional, Tuple

from bs4 import BeautifulSoup, SoupStrainer
from loguru import logger
from path import Path
from pydantic import ValidationError

from ..island import Island, other_to_accession

table_subset = SoupStrainer("table")


def get_coordinate(info: dict) -> Tuple[int, int]:
    """Extract the island coordinates (start / end) from the raw ICEberg data.

    Args:
        info (dict): the data extracted from the ICEberg page

    Returns:
        Tuple[Optional[int], Optional[int]]: if the coordinate is found, return start / end as integer.
    """
    try:
        # The key is not stable in the different pages : can be "coordinate", "Gene coordinate"...
        coord = [coord for key, coord in info.items() if "coordinate" in key.lower()][0]
        start, end = map(int, coord.split(".."))
        return start, end
    except IndexError:
        return None, None


def iceberg_page_to_island(file: str) -> Optional[Island]:
    """Extract the data from one ICEberg web page, and return the corresponding Island

    Args:
        file (str): the ICEberg web page to parse.

    Returns:
        Island
    """
    with open(file, "r") as f:
        html_tables = BeautifulSoup(f, "lxml").select("table")
    if len(html_tables) <= 5:
        return None

    table = html_tables[0].select("td")
    references = html_tables[4].select("a")
    references = ",".join(
        [ref.text.strip().replace("PudMed", "PMID") for ref in references]
    )

    info_list = [col.text.strip() for col in table]
    info = dict(zip(info_list[::2], info_list[1::2]))

    start, end = get_coordinate(info)
    organism = info["Organism"]
    accession = other_to_accession(info["Nucleotide Sequence"])
    insertion = info["Insertion site"]
    if insertion == "-":
        insertion = ""
    other = dict(name=info["Name"], family=info["Family"], function=info["Function"])
    other = {key: value for key, value in other.items() if value != "-"}

    island = Island(
        detection="ICEberg",
        start=start,
        end=end,
        organism=organism,
        accession=accession,
        insertion=insertion,
        reference=references,
        other=json.dumps(other),
    )
    return island


def parse_iceberg_files(path):
    """Create a generator that will parse some ICEberg pages and will yield
    the correspondant Islands.

    Args:
        path (str): The place where lie the ICEberg pages.

    Yield: Island
    """
    path = Path(path)
    for file in path.files("*.html"):
        try:
            island = iceberg_page_to_island(file)
        except ValidationError as e:
            logger.warning(e)
            continue
        if island:
            yield island
