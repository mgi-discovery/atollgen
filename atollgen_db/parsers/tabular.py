#!/usr/bin/env python
# coding=utf-8

"""
>>> for island in parse_islandviewer_files("../data/inputs/all_gis_islander_iv4.txt"):
...     print(island)
"""


import json
from functools import partial

import pandas as pd

from ..island import Island


def parse_csv(file: str, columns=None, chunksize=None, *args, **kwargs):
    chunks = pd.read_csv(file, chunksize=chunksize, *args, **kwargs)

    if not chunksize:
        chunks = [chunks]

    for df in chunks:
        if columns is not None:
            df = df[columns]
        for row in df.to_dict("records"):
            yield Island(**row)


parse_iv4 = partial(
    parse_csv, skiprows=1, names=["accession", "start", "end", "detection"]
)


def parse_jlao_db(filename):
    def to_island(row):
        accession = row["GenBank nuccore accession"]
        start = row["Left end coordinate"]
        end = row["Right end coordinate"]
        other = {"name": row["Element name"], "strain": row["Strain"]}
        return Island(
            accession=accession,
            start=start,
            end=end,
            other=json.dumps(other),
            detection="jlao",
        )

    df = (
        pd.read_excel(filename, na_values=["-"], skiprows=1)
        .dropna(how="all", axis=1)
        .dropna(how="any", axis=0)
    )
    df = df.apply(pd.to_numeric, errors="ignore", downcast="integer")
    df["GenBank nuccore accession"] = (
        df["GenBank nuccore accession"].str.split(".").str.get(0)
    )
    yield from df.apply(to_island, axis=1)
