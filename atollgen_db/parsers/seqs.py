import re

from Bio import SeqIO

from ..island import Island

ICEV2_PAT = re.compile(
    r"ICEberg\|\d+\|.*?\|GenBank\|(?P<accession>.*?)\|(?P<start>\d+)..(?P<end>\d+)"
)


def parse_iceberg_v2(filename):
    for rec in SeqIO.parse(
        filename,
        format="fasta",
    ):
        yield Island(**ICEV2_PAT.match(rec.id).groupdict(), detection="iceberg_v2")
