from functools import cached_property
from operator import ge
from typing import Callable, Dict, Iterable, Sequence, Union

import pandas as pd
from entrez_fetcher import GenomeStore
from pandas import DataFrame, concat
from path import Path

from .island import Island


def _null_progress(it: Iterable, *args, **kwargs):
    return it


class AtollgenDatabase:
    ISLANDS_DIRNAME = "islands"

    def __init__(self, path: str, fresh=False, genome_store=None, progress=None):
        """AtollgenDatabase, on-disk database that store islands data that come from different
        sources on disk and allow to easily manipulate, modify and update them.

        It gives access to the islands as a pandas.DataFrame as well as a list of atollgen.Island.

        Args:
            path (str): path to the atollgen database folder.
            genome_store (Optional[GenomeStore]): an instance of GenomeStore. If None,
                one will becreated. It is strongly recommended to furnish one with your
                `email` and `api_key`. That way, you will be allowed to run up to 10
                concurrent request and speed up the fasta fetching.
            fresh (bool, optional): Defaults to False. if True, recreate the database
                from scratch.
        """
        self._path = path
        if fresh:
            Path(self.path).rmtree_p()
        self.islands_dir.makedirs_p()
        if progress is None:
            try:
                from tqdm.auto import tqdm

                progress = tqdm
            except ImportError:
                progress = _null_progress
        self.progress = progress
        if genome_store is None:
            genome_store = GenomeStore(self.path / "genome_store", progress=progress)
        self.genome_store = genome_store
        self._raw_islands_cache = {}

    def clear_cache(self, key="all"):
        available_keys = ["docsums", "merged_islands", "taxonomy"]
        if key == "all":
            for cache in available_keys:
                self.clear_cache(cache)
            return

        if key not in available_keys:
            raise ValueError(
                f"{key} is not a cache. Allowed values are: {available_keys}"
            )
        try:
            delattr(self, key)
        except AttributeError:
            pass

    @property
    def raw_islands(self) -> DataFrame:
        """Return the islands that live on-disk as a DataFrame.

        Returns:
            DataFrame
        """
        sources = self.sources
        if not sources:
            return pd.DataFrame(columns=Island.columns)
        return concat(
            (self.by_source(source) for source in sources),
            keys=sources,
            names=["source"],
        ).sort_index()

    @cached_property
    def islands(self) -> DataFrame:
        """Return the islands that live on-disk as a DataFrame.

        Returns:
            DataFrame
        """
        islands = self.raw_islands
        docsums = self.docsums
        taxons = self.taxonomy
        islands = islands.join(
            docsums[["tax_id", "length"]].rename(columns={"length": "host_size"}),
            on="accession",
        )
        islands = islands.join(taxons.scientific_name, on="tax_id")
        islands["organism"] = (
            islands["organism"].replace("", None).combine_first(islands.scientific_name)
        )
        islands = islands.drop("scientific_name", axis=1)
        return islands

    @property
    def __len__(self) -> int:
        return self.raw_islands.__len__

    @property
    def sources(self) -> Sequence[str]:
        return [fn.basename().stripext() for fn in self.islands_dir.files("*.pq")]

    def _fetch_docsums(self, accessions=None) -> int:
        if accessions is None:
            accessions = self.raw_islands.accession.to_list()
        self.genome_store.load_docsums(accessions)

    @cached_property
    def docsums(self):
        self._fetch_docsums()
        df = self.genome_store.docsums.set_index("caption")
        return df[df.index.isin(self.raw_islands.accession)]

    def _fetch_taxonomy(self, accessions=None) -> int:
        if accessions is None:
            accessions = self.raw_islands.accession.to_list()
        self.genome_store.load_taxonomy(accessions)

    @cached_property
    def taxonomy(self):
        self._fetch_taxonomy()
        df = self.genome_store.taxonomies.set_index("tax_id")
        return df[df.index.isin(self.docsums.tax_id)]

    # All the paths of the on-disk data
    @property
    def islands_dir(self) -> Path:
        return Path(self.path) / self.ISLANDS_DIRNAME

    @property
    def path(self) -> Path:
        return Path(self._path)

    def by_source(self, source: str) -> DataFrame:
        """Returns:
        DataFrame: A dataframe that contain the original data (that come directly from the sources).
        """
        if source in self._raw_islands_cache:
            return self._raw_islands_cache[source]
        df = pd.read_parquet(self.islands_dir / f"{source}.pq")
        self._raw_islands_cache[source] = df
        return df

    # Routine that allow to add / update / remove data on-disk
    def add_source(
        self,
        parser: Iterable[Island],
        name: str,
        force=False,
        **kwargs: Dict[str, Union[str, Callable]],
    ) -> None:
        """Add a source to the database. The source can be any iterable that yield an Island.
        The name is mandatory and can be any string but `update` which is a reserved name.

        If an other source exists with the same name, that function will do nothing and return None
        unless `force` is True. In that case, the previous source will be overwritten.

        Args:
            parser (Iterable[Island]):
            name (str): the name of the source.
            force (bool, optional): Defaults to False. if True, overwrite a source with the same name.
            **kwargs (Union[str, Callable]): Every extra named parameters can be use to
                add / overwrite a column of the source. It can be either a simple value
                (and all the
                row will have that value), or a callable that will take a row of the
                islands DataFrame and return the value of this column for each row.

        Raises:
            KeyError: raised if a reserved name is provided.

        Examples:
            Add all the row of the Islander Database

            >>> from ilotgen import IlotgenDB
            >>> from ilotgen.parsing.sql import parse_islander_db
            >>> ilotgenDB = IlotgenDB("./ilotgen_db/")
            >>> ilotgenDB.add_source(parse_islander_db("./islander.db"), name="islander")

            Same, but overwrite the type_ column with a single value

            >>> ilotgenDB.add_source(parse_islander_db("./islander.db"), name="islander_overwritted", type_="test_value")
            >>> ilotgenDB.by_source("islander_overwritten").iloc[0].type_
            'test_value'
        """
        parquet_dir = self.islands_dir / f"{name}.pq"
        if parquet_dir.exists() and not force:
            raise FileExistsError()
        df = Island.to_df(self.progress(parser, desc=f"Parsing {name}"))
        for key, value in kwargs.items():
            if callable(value):
                value = df.apply(value, axis=1)
            df[key] = value
        df.sort_values(["accession", "start", "end"], ignore_index=True, inplace=True)
        df.to_parquet(parquet_dir)
        self._raw_islands_cache[name] = df
        self.clear_cache()

        return

    def merge_islands(self, islands=None):
        if islands is None:
            islands = self.islands
        islands = islands.drop("other", axis=1).reset_index()
        grp = islands.sort_values(["accession", "start", "end"]).groupby(
            ["accession", "start", "end"]
        )

        def join_if_not_unique(values):
            unik_values = set([value for value in values if value])
            return ";".join(map(str, unik_values))

        def join_keeping_all(values):
            return ";".join(map(str, values))

        aggregate_cols = {
            col: join_keeping_all
            if col in ["index", "source", "detection"]
            else join_if_not_unique
            for col in islands.columns
        }
        merged_islands = grp.agg(aggregate_cols).drop(
            ["accession", "start", "end"], axis=1
        )
        merged_islands["duplicates"] = grp.organism.size()
        return merged_islands

    @cached_property
    def merged_islands(self):
        return self.merge_islands(self.islands)

    def remove_source(self, name: str):
        if name in self.sources:
            (self.islands_dir / f"{name}.pq").remove()
        if name in self._raw_islands_cache:
            del self._raw_islands_cache[name]
        self.clear_cache()

    def download_genomes(self, accessions=None, max_batch_size=None):
        if accessions is None:
            accessions = self.raw_islands.accession.to_list()
        self.genome_store.load_sequences(accessions, max_batch_size=max_batch_size)

    def export_database(
        self,
        output_dir: Path,
        split_files_by_source=True,
        force=False,
    ):
        raise NotImplementedError

    def __repr__(self):
        return self.islands.__repr__()

    def _repr_html_(self):
        return self.islands._repr_html_()

    def _repr_latex_(self):
        return self.islands._repr_latex_()
